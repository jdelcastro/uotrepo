#!/bin/sh
####################################
#
# Start services for Ueb Of Things Platform.
#
####################################

########
# Begins to Restart Rabbit MQ Broker
#######
invoke-rc.d rabbitmq-server stop
invoke-rc.d rabbitmq-server start

#Start Rabbit MQ RPC Server
echo "Starting RPC Server..."
php app/console rabbitmq:rpc-server notification_keeper &


#Start Rabbit MQ Consumer
echo "Starting Rabbit MQ Consumers..."
php app/console rabbitmq:consumer -w consume_notification_web  &
php app/console rabbitmq:consumer -w consume_notification_mail &
php app/console rabbitmq:consumer -w consume_notification_sms &

#start Clank Server
php app/console clank:server 
 
