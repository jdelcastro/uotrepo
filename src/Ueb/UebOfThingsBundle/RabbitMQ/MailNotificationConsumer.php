<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailNotificationConsumer
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\RabbitMQ;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use PhpAmqpLib\Message\AMQPMessage;

class MailNotificationConsumer implements ConsumerInterface {

    private $mailer;
    private $userRepository;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, $userRepository, EngineInterface $templating) {
        $this->mailer = $mailer;
        $this->userRepository = $userRepository;
        $this->templating = $templating;
    }

    public function execute(AMQPMessage $msg) {

        try {

            $message = unserialize($msg->body);
            $this->broadcastMail($message);
        } catch (Exception $ex) {
            
        }
    }

    protected function broadcastMail($message) {


        $subject = "[UebOfThings] " . $message['feed_name'] . "::";

        if ($message['notification_type'] == 'warning') {
            $subject.= "Valor alerta";
        } else {
            $subject.= "Valor crítico";
        }

        $bodyview = $this->templating->render(
                'UebUebOfThingsBundle:mails:mail.txt.twig', array('message' => $message)
        );

        $users = $this->userRepository->findAll();

        foreach ($users as $user) {

            $mailTo = $user->getEmail();


            if ($mailTo != NULL) {

                if (!$this->mailer->getTransport()->isStarted()) {
                    $this->mailer->getTransport()->start();
                }

                $email_message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom("uothings@gmail.com")
                        ->setTo($mailTo)
                        ->setBody($bodyview)
                        ->setContentType("text/html");

                $this->mailer->send($email_message);
                $this->mailer->getTransport()->stop();
            }
        }
    }

}
