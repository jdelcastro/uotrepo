<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationConsumer
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\RabbitMQ;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

use Monolog\Logger;  
use Monolog\Handler\StreamHandler;  

class WebNotificationConsumer implements ConsumerInterface  {
    
    private $container;
    
    public function __construct($container) {
        $this->container = $container;
    }
    
    
    
    public function execute(AMQPMessage $msg) {
        try {
            $message = unserialize($msg->body); 
            $client = $this->container->get("old_sound_rabbit_mq.check_notification_rpc");
            $client->addRequest(serialize(array(
                "action" => "push",
                "notification" => $message
            )), 'notification_keeper', 'set_id');
            
            $replies = $client->getReplies();
            $response = $replies['set_id'];
        } catch (Exception $ex) {
        }
    }

    
}
