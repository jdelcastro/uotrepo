<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationKeeper
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\RabbitMQ;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use SplQueue;

class NotificationKeeper implements ConsumerInterface {

    private $queue;

    public function __construct() {
        $this->queue = new SplQueue();
    }

    public function execute(\PhpAmqpLib\Message\AMQPMessage $msg) {
        $message = unserialize($msg->body);
        $action = $message["action"];


        switch ($action) {
            case "push":
                $notification = $message["notification"];
                $this->queue->push($notification);
                return serialize($this->queue->top());
            case "pop":
                if (!$this->queue->isEmpty()) {
                    return serialize($this->queue->pop());
                }

            default:
                break;
        }
    }

}
