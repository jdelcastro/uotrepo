<?php

namespace Ueb\UebOfThingsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BoardType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array("label"=>"Nome"))
            ->add('description', 'textarea', array("label"=>"Descrição"))
            ->add('arduino')
            ->add('type')          
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ueb\UebOfThingsBundle\Entity\Board'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ueb_uebofthingsbundle_board';
    }
}
