<?php

namespace Ueb\UebOfThingsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Collection;

class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('celNumber', 'text', array(
                    "label" => "Telemóvel"
                ))
                ->add('email', 'email', array(
                    'label' => "E-mail"
                ))
                ->add('save', 'submit', array(
                    "label" => "Gravar"
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        
        $resolver->setDefaults(array(
            'data_class' => 'Ueb\UebOfThingsBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ueb_uebofthingsbundle_user';
    }

}
