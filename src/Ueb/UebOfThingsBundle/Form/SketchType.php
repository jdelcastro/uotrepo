<?php

namespace Ueb\UebOfThingsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SketchType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'Nome/Título'))
            ->add('code', 'textarea', array('label' => 'Codigo - Fonte'))
            ->add('program', 'submit')
            ->add('cancel', 'submit', array(
                    // disable validation
                     'validation_groups' => false )
                )
            ->add('upload', 'submit', array(
                    'disabled' => true
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ueb\UebOfThingsBundle\Entity\Sketch'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ueb_uebofthingsbundle_sketch';
    }
}
