<?php

namespace Ueb\UebOfThingsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Ueb\UebOfThingsBundle\Form\FeedType;

class FeedGroupType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('feeds')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ueb\UebOfThingsBundle\Entity\FeedGroup'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ueb_uebofthingsbundle_feedgroup';
    }
}
