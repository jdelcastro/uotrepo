<?php

namespace Ueb\UebOfThingsBundle\Form;

use Ueb\UebOfThingsBundle\Entity\Feed;
use Ueb\UebOfThingsBundle\Entity\Thing;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class FeedType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array("label" => "Nome"))
                ->add('mac')
                ->add('measureunit', 'text', array("label" => "Unidade de Medida"))
                
                ->add('board', 'entity', array(
                    'class' => 'UebUebOfThingsBundle:Board',
                    'query_builder' => function(EntityRepository $er) {
                                return $er->findAllDefaultBoards("query_builder");
                            }
                ))
                ->add('thing')
                
        ;
                
        $formModifier = function (FormInterface $form, Thing $thing = null) {

            $form->remove('save');
            if ($thing != null) {

                if ($thing->getType() === Thing::ACTUATOR) {
                    $form
                            ->remove('warningValue')
                            ->remove('criticalVaue');
                } else if ($thing->getType() === Thing::SENSOR) {
                    $form
                            
                            ->add('warningValue', 'text', array("label" => "Valor de Alarme"))
                            ->add('criticalVaue', 'text', array("label" => "Valor crítico"))
                                    ->add('save', 'submit', array(
                                'label' => "Gravar")
                            );
                }
            }
            $form->add('save', 'submit', array(
                    'label' => "Gravar")
            );
        };
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $feed = $event->getData();
                
                if($feed!=null) {
                    $formModifier($event->getForm(), $feed->getThing());
                } else {
                    $formModifier($event->getForm());
                }
            }
        );
                
        $builder->get('thing')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formModifier) {
            
            $thing = $event->getForm()->getData();
            
            $formModifier($event->getForm()->getParent(), $thing);
            
        });
        
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ueb\UebOfThingsBundle\Entity\Feed'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ueb_uebofthingsbundle_feed';
    }

}
