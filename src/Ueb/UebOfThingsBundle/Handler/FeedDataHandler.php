<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FeedDataHandler
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\EntityManager;
use Ueb\UebOfThingsBundle\Entity\Feed;
use Ueb\UebOfThingsBundle\Entity\FeedData;

class FeedDataHandler {

    private $manager;

    public function __construct(EntityManager $manager) {
        $this->manager = $manager;
    }

    public function handleRequest(Request $request) {

        try {
            $this->handleData($request->getContent());
        } catch (Exception $ex) {
            throw new HttpException(400, "Whoops! Looks like there is invalid parameters. :/");
        }
    }

    private function handleData($data) {

        $jsonIterator = new \RecursiveIteratorIterator(
                new \RecursiveArrayIterator(json_decode($data, TRUE)), \RecursiveIteratorIterator::SELF_FIRST);

        foreach ($jsonIterator as $key => $val) {
            if (!is_array($val)) {

                $feed = $this->manager->getRepository('UebUebOfThingsBundle:Feed')->find($key);
                if ($feed) {
                    $feeddata = new FeedData();
                    $feeddata->setValue($val);
                    $feed->addDatum($feeddata);
                    $this->manager->persist($feed);
                    $this->manager->flush();
                    $this->manager->clear();
                }
            }
        }
    }

}
