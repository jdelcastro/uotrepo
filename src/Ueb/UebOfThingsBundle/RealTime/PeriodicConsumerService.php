<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PeriodicConsumers
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\RealTime;

use Symfony\Component\Process\Process;
use JDare\ClankBundle\Periodic\PeriodicInterface;
use PhpAmqpLib\Exception\AMQPIOException;

class PeriodicConsumerService implements PeriodicInterface {

    private $container;
    private $pusher;

    public function __construct($container, $pusher) {
        $this->pusher = $pusher;
        $this->container = $container;
    }

    public function tick() {

        try {
            $client = $this->container->get("old_sound_rabbit_mq.check_notification_rpc");
            $client->addRequest(serialize(array(
                "action" => "pop"
                    )), 'notification_keeper', 'get_id');
            $replies = $client->getReplies();
            $response = unserialize($replies['get_id']);
            if($response) {
                $this->pusher->broadcast($response);
            }
            
        } catch (\PhpAmqpLib\Exception\AMQPRuntimeException $ex) {
            
            
        }
    }

}
