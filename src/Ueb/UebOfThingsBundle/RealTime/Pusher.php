<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pusher
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\RealTime;

use Ratchet\ConnectionInterface as Conn;



class Pusher implements \Ratchet\Wamp\WampServerInterface {

    private $subscribedNotifications;
    
    public function __construct() {
        $this->subscribedNotifications = array() ;
    }
  

    public function onNotification($notification) {

        if (array_key_exists('ueb', $this->subscribedNotifications)) {
            $topic = $this->subscribedNotifications['ueb'];

            $topic->broadcast($notification);
        }
        
        
    }

    public function onSubscribe(Conn $conn, $topic) {
        
        if (!array_key_exists($topic->getId(), $this->subscribedNotifications)) {
            $this->subscribedNotifications[$topic->getId()] = $topic;
        }
        
        $topic->broadcast(json_encode($this->subscribedNotifications));
    }
    
    
    
    public function broadcast($notification){
        if (array_key_exists('ueb', $this->subscribedNotifications)) {
            $this->subscribedNotifications['ueb']->broadcast(json_encode($notification));
        }
        
    }

    

    //**************************************************************
    //****** METHODS NOT USED, JUST FOR THE INTERFACE IMPLEMENTATION
    //**************************************************************
    public function onClose(Conn $conn) {
        
    }

    public function onError(Conn $conn, \Exception $e) {
        
    }

    public function onOpen(Conn $conn) {
        
    }

    public function onPublish(Conn $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        
        $conn->close();
    }

    public function onCall(\Ratchet\ConnectionInterface $conn, $id, $topic, array $params) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onUnSubscribe(Conn $conn, $topic) {
        
    }

    

}
