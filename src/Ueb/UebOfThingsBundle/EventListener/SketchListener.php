<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CodeLoader
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Ueb\UebOfThingsBundle\Entity\Sketch;

class SketchListener {
    
    private $arduino_tools;
    
    public function postLoad(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        
        if ($entity instanceof Sketch) {
            if (!empty($entity->getBoard())) {
                $code = $this->arduino_tools->getSourceCode($entity->getBoard()->getName());
                $entity->setCode($code);
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Sketch) {
            $this->arduino_tools->clean($entity->getBoard()->getName());
        }
    }
    
    public function setArduinoToolsService($service) {
        $this->arduino_tools = $service;
    }

}
