<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationListener
 *
 * @author joao
 */
namespace Ueb\UebOfThingsBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Ueb\UebOfThingsBundle\Entity\Notification;

class NotificationListener {
    
    private $container;
    
    public function __construct($container) {
        $this->container = $container;
    }
    
    public function postPersist(LifecycleEventArgs $args) {
        
        $entity = $args->getEntity();
        
        if($entity instanceof Notification) {
            
            $users = $this->container->get("doctrine.orm.entity_manager")->getRepository('UebUebOfThingsBundle:User')->findAll();
            foreach ($users as $user) {
                $user->addNotification($entity);
                $this->container->get("doctrine.orm.entity_manager")->persist($entity);
            }
            $this->container->get("doctrine.orm.entity_manager")->flush();
        }
        
    }
    
}
