<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FeedListener
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Ueb\UebOfThingsBundle\Entity\FeedData;
use Ueb\UebOfThingsBundle\Entity\Notification;

class FeedListener {

    private $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function postPersist(LifecycleEventArgs $args) {


        $entity = $args->getEntity();

        if ($entity instanceof FeedData) {

            $feed = $entity->getFeed();
            $producer = $this->container->get("old_sound_rabbit_mq.publish_notification_producer");

            //Verificar se valor de alarme
            if ($entity->getValue() >= $feed->getWarningValue() && $entity->getValue() < $feed->getCriticalVaue()) {
                $message = serialize(array(
                    'feed_id' => $feed->getId(),
                    'feed_name' => $feed->getName(),
                    'value' => $entity->getValue(),
                    'measure' => $feed->getMeasureunit(),
                    'notification_type' => 'warning',
                    'feed_url' => $this->container->get('router')->generate("feed_show", array("id"=> $feed->getId()), true)
                ));
                $producer->publish($message, 'notifications');
                
                $this->persistNotification($entity, Notification::WARNING);
                
            } else //verificar se valor crítico 
            if ($entity->getValue() >= $feed->getCriticalVaue()) {
                $message = serialize(array(
                    'feed_id' => $feed->getId(),
                    'feed_name' => $feed->getName(),
                    'value' => $entity->getValue(),
                    'measure' => $feed->getMeasureunit(),
                    'notification_type' => 'critical',
                    'feed_url' => $this->container->get('router')->generate("feed_show", array("id"=> $feed->getId()), true)
                ));
                $producer->publish($message, 'notifications');
                
                $this->persistNotification($entity, Notification::CRITICAL);
            }
        }
    }
    
    private function persistNotification($feedData, $ntype){
        
        $notification = new Notification();
        $notification->setFeeddata($feedData);
        $notification->setType($ntype);
        
        $this->container->get("doctrine.orm.entity_manager")->persist($notification);
        $this->container->get("doctrine.orm.entity_manager")->flush();
        
    }

}
