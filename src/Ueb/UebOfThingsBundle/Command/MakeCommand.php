<?php

namespace Ueb\UebOfThingsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MakeCommand extends ContainerAwareCommand 
{
    protected function configure() {
        $this
            ->setName('arduino-tools:make')
            ->setDescription('Build Arduino Sketch')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) {

        
        $output->writeln("Teste makefile");
        
    }
}
