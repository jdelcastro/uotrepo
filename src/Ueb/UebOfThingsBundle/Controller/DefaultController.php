<?php

namespace Ueb\UebOfThingsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ueb\UebOfThingsBundle\Form\UserType;

/**
 * 
 * @Route("/")
 */
class DefaultController extends Controller
{
    
    public function indexAction()
    {
        //Esta secção poderia ser mais eficiente com uma query a BD mas para ja assim ficará
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository("UebUebOfThingsBundle:Feed");
        $feeds = $repository->findAll();
        $stats = $this->feedStats($repository, $feeds);
        // 
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard");
        return $this->render('UebUebOfThingsBundle:Default:index.html.twig', 
                array(
                    'normal' => $stats['normal'],
                    'warning' => $stats['warning'],
                    'critical' => $stats['critical']
                )
        );
    }
    
    
    /**
     * 
     * @Route("login", name="login")
     */
    public function loginAction(){
        $request = $this->getRequest();
        $session = $request->getSession();
 
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Login");
 
        return $this->render('UebUebOfThingsBundle:Default:login.html.twig', array(
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        ));
    }
    
    /**
     * @Route("login_check", name="login_check")
     */
    public function loginCheckAction()
    {

    }
    
    /**
     * @Route("logout", name="logout")
     */
    public function logoutAction(){
        
    }
    
    
    /**
     * 
     * @Route("clean_notifications", name="clean_notifications")
     * @Method("POST")
     */
    public function cleanNotificationsAction() {
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        $user->removeNotifications();
        
        $em->persist($user);
        $em->flush();
        
        return new Response("ok", Response::HTTP_OK);
        
    }
    
    
    /**
     * 
     * @Route("settings", name="user_settings")
     * @Method("GET")
     * @Template()
     */
    public function settingsAction() {
        
        $user = $this->getUser();
        
        $settingsForm = $this->createForm(new UserType(), $user, array(
            'action' => $this->generateUrl('user_settings_update'),
            'method' => 'PUT',
        ));
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Definições");
        
        return array(
            'form' => $settingsForm->createView()
        );
        
    }
    
    /**
     *
     *
     * @Route("settings", name="user_settings_update")
     * @Method("PUT")
     * @Template("UebUebOfThingsBundle:Board:edit.html.twig")
     */
    public function settingsUpdateAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        
        $settingsForm = $this->createForm(new UserType(), $user, array(
            'action' => $this->generateUrl('user_settings_update'),
            'method' => 'PUT',
        ));

        $settingsForm->handleRequest($request);

        if ($settingsForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('user_settings'));
        }

        return array(
            'form'      => $settingsForm->createView()
        );
    }
    
    
    /*
     * aux function
     * return array
     * 
     * returns an indexed array["normal"=>x, "warning"=>y , "critical"=>z]
     */
    private function feedStats($repository, $feed_array) {
        
        $normal = 0;
        $warning = 0;
        $critical = 0;


        foreach ($feed_array as $f) {
            $feed = $repository->findFeedJoinedByFeedData($f->getId(), 'hour');
            if ($feed != null) {
                $normal+= $feed->getCountNormalValues();
                $warning+= $feed->getCountWarningValues();
                $critical+= $feed->getCountCriticalValues();
            }
        }

        return array(
            'normal' => $normal,
            'warning' => $warning,
            'critical' => $critical
        );
    }

}
