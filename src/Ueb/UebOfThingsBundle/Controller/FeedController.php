<?php

namespace Ueb\UebOfThingsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ueb\UebOfThingsBundle\Entity\Feed;
use Ueb\UebOfThingsBundle\Form\FeedType;

/**
 * Feed controller.
 *
 * @Route("/feed")
 */
class FeedController extends Controller {

    /**
     * Lists all Feed entities.
     *
     * @Route("/", name="feeds")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $sensors = $em->getRepository('UebUebOfThingsBundle:Feed')->findAllSensors();
        $actuators = $em->getRepository('UebUebOfThingsBundle:Feed')->findAllActuators();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("homepage"));
        $breadcrumbs->addItem("Things / Feeds");

        return array(
            'sensors' => $sensors,
            'actuators' => $actuators
        );
    }

    /**
     * Creates a new Feed entity.
     *
     * @Route("/", name="feed_create")
     * @Method("POST")All();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("homepage"));
        $breadcrumbs->addItem("Things / Feeds");

        return array(
     * @Template("UebUebOfThingsBundle:Feed:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Feed();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('feed_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Feed entity.
     *
     * @param Feed $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Feed $entity) {
        $form = $this->createForm(new FeedType(), $entity, array(
            'action' => $this->generateUrl('feed_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Feed entity.
     *
     * @Route("/new", name="feed_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Feed();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Things / Feeds", $this->generateUrl('feeds'));
        $breadcrumbs->addItem("Novo Feed");

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Feed entity.
     *
     * @Route("/{id}", name="feed_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Feed')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Feed entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Things / Feeds", $this->generateUrl('feeds'));
        $breadcrumbs->addItem($entity->getName());
        
      

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Feed entity.
     *
     * @Route("/{id}/edit", name="feed_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Feed')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Feed entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Things / Feeds", $this->generateUrl('feeds'));
        $breadcrumbs->addItem($entity->getName(), $this->generateUrl('feed_show', array('id' => $entity->getId())));
        $breadcrumbs->addItem("Editar");

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Feed entity.
     *
     * @param Feed $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Feed $entity) {
        $form = $this->createForm(new FeedType(), $entity, array(
            'action' => $this->generateUrl('feed_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        
        $form->remove('thing');

        return $form;
    }

    /**
     * Edits an existing Feed entity.
     *
     * @Route("/{id}", name="feed_update")
     * @Method("PUT")
     * @Template("UebUebOfThingsBundle:Feed:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Feed')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Feed entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('feed_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Feed entity.
     *
     * @Route("/{id}", name="feed_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UebUebOfThingsBundle:Feed')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Feed entity.');
            }

            
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('feeds'));
    }

    /**
     * Creates a form to delete a Feed entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('feed_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Get all Feed Data
     * 
     * @Route(
     *  "/{id}/data/{period}.{_format}",
     *  defaults={"_format" = "json" },
     *  requirements = { "_format" = "html|json", "period" = "hour|day|week|month" },
     *  name="feed_data"
     * )
     * @Template()
     */
    public function feedsDataAction($id, $period) {
        $em = $this->getDoctrine()->getManager();
        $repository= $em->getRepository("UebUebOfThingsBundle:Feed");
        $feed = $repository->findFeedJoinedByFeedData($id, $period);

        if (!$feed) {
            $feed_temp = $repository->find($id);

            if (!$feed_temp) {
                throw $this->createNotFoundException('Unable to find Feed entity.');
            }
            
            $feed = new Feed();
            $feed->setMeasureunit($feed_temp->getMeasureUnit());
        }
        
        return array(
            'feed' => $feed,
            'period' => $period,
            'last_value' => $feed->getCurrentData()
        );
    }
    

}
