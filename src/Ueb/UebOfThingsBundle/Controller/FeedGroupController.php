<?php

namespace Ueb\UebOfThingsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ueb\UebOfThingsBundle\Entity\FeedGroup;
use Ueb\UebOfThingsBundle\Form\FeedGroupType;

/**
 * FeedGroup controller.
 *
 * @Route("/feedgroup")
 */
class FeedGroupController extends Controller
{

    /**
     * Lists all FeedGroup entities.
     *
     * @Route("/", name="feedgroup")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UebUebOfThingsBundle:FeedGroup')->findAll();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("homepage"));
        $breadcrumbs->addItem("Things / Feeds");

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new FeedGroup entity.
     *
     * @Route("/", name="feedgroup_create")
     * @Method("POST")
     * @Template("UebUebOfThingsBundle:FeedGroup:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new FeedGroup();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $this->saveRelatedFeeds($entity, $em, TRUE);
            $em->flush();

            return $this->redirect($this->generateUrl('feedgroup_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a FeedGroup entity.
     *
     * @param FeedGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(FeedGroup $entity)
    {
        $form = $this->createForm(new FeedGroupType(), $entity, array(
            'action' => $this->generateUrl('feedgroup_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new FeedGroup entity.
     *
     * @Route("/new", name="feedgroup_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new FeedGroup();
        $form   = $this->createCreateForm($entity);
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("homepage"));
        $breadcrumbs->addItem("Things / Feeds");

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a FeedGroup entity.
     *
     * @Route("/{id}", name="feedgroup_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:FeedGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FeedGroup entity.');
        }
        

        return array(
            'entity'      => $entity
        );
    }

    /**
     * Displays a form to edit an existing FeedGroup entity.
     *
     * @Route("/{id}/edit", name="feedgroup_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:FeedGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FeedGroup entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("homepage"));
        $breadcrumbs->addItem("Things / Feeds");

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a FeedGroup entity.
    *
    * @param FeedGroup $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FeedGroup $entity)
    {
        $form = $this->createForm(new FeedGroupType(), $entity, array(
            'action' => $this->generateUrl('feedgroup_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FeedGroup entity.
     *
     * @Route("/{id}", name="feedgroup_update")
     * @Method("PUT")
     * @Template("UebUebOfThingsBundle:FeedGroup:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:FeedGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FeedGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            $this->saveRelatedFeeds($entity, $em, TRUE);
            $em->flush();

            return $this->redirect($this->generateUrl('feedgroup_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a FeedGroup entity.
     *
     * @Route("/{id}", name="feedgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UebUebOfThingsBundle:FeedGroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FeedGroup entity.');
            }

            $this->saveRelatedFeeds($entity, $em, FALSE);
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('feedgroup'));
    }

    /**
     * Creates a form to delete a FeedGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('feedgroup_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    //aux function to update feed-group associations
    // $flag true for update/create, false removal operation
    private function saveRelatedFeeds($entity, $manager, $flag) {
        
        
        foreach ($entity->getFeeds() as $feed) {
            
            
            if ($flag) {
                $feed->setGroup($entity);
            } else {
                $feed->setGroup();
            }
            $manager->persist($feed);
        }
        
    }
}
