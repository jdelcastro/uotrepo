<?php

namespace Ueb\UebOfThingsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;  
use Ueb\UebOfThingsBundle\Handler\FeedDataHandler;

/** 
 * Things Api Controller
 * 
 * @Route("/thingsapi", service="things_api.controller")
 */
class ThingsApiController
{
 
    
    private $feeddatahandler;
    private $em;
    
    public function __construct(FeedDataHandler $feeddatahandler, $entityManager) {
        $this->feeddatahandler = $feeddatahandler;
        $this->em = $entityManager;
    }
    
    /**
     * 
     * @return type
     * 
     * @Route("/{board}", name="get_reles")
     * @Method("GET")
     */
    public function getRelesAction(Request $request, $board) {
        
       
        
        return $em->getRepository('UebUebOfThingsBundle:FeedData')->findAll();
        
    }
    
    /**
     * 
     * @Route("/feeds.{_format}", 
     *  defaults={"_format" = "json" },
     *  requirements = { "_format" = "json"},
     *  name="put_feeds"
     * )
     * @Method("PUT")
     * 
     */
    public function feedStateAction(Request $request) {
        
        $this->feeddatahandler->handleRequest($request);
        
        return new Response("Done", Response::HTTP_OK);
        
    }
    
    
    
}
