<?php

namespace Ueb\UebOfThingsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ueb\UebOfThingsBundle\Entity\Board;
use Ueb\UebOfThingsBundle\Form\BoardType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Board controller.
 *
 * @Route("/board")
 */
class BoardController extends Controller
{

    /**
     * Lists all Board entities.
     *
     * @Route("/", name="board")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        $defaults = $em->getRepository('UebUebOfThingsBundle:Board')->findAllDefaultBoards();
        $isps = $em->getRepository('UebUebOfThingsBundle:Board')->findAllISPBoards();
        
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("homepage"));
        $breadcrumbs->addItem("Boards");

        return array(
            'defaults' => $defaults,
            'isps' => $isps
        );
    }
    /**
     * Creates a new Board entity.
     *
     * @Route("/", name="board_create")
     * @Method("POST")
     * @Template("UebUebOfThingsBundle:Board:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Board();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('board_show', array('name' => $entity->getName())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Board entity.
    *
    * @param Board $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Board $entity)
    {
        $form = $this->createForm(new BoardType(), $entity, array(
            'action' => $this->generateUrl('board_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Board entity.
     *
     * @Route("/new", name="board_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Board();
        $form   = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Boards", $this->generateUrl('board'));
        $breadcrumbs->addItem("Nova Placa");
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Board entity.
     *
     * @Route("/{name}", name="board_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($name)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Board')->findOneByName($name);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Board entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Boards", $this->generateUrl('board'));
        $breadcrumbs->addItem($entity->getName());
        
        $deleteForm = $this->createDeleteForm($name);
        
        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Board entity.
     *
     * @Route("/{name}/edit", name="board_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($name)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Board')->findOneByName($name);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Board entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($name);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Boards", $this->generateUrl('board'));
        $breadcrumbs->addItem($entity->getName(), $this->generateUrl('board_show',array('name' => $entity->getName())));
        $breadcrumbs->addItem("Editar");
        
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Board entity.
    *
    * @param Board $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Board $entity)
    {
        $form = $this->createForm(new BoardType(), $entity, array(
            'action' => $this->generateUrl('board_update', array('name' => $entity->getName())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Board entity.
     *
     * @Route("/{name}", name="board_update")
     * @Method("PUT")
     * @Template("UebUebOfThingsBundle:Board:edit.html.twig")
     */
    public function updateAction(Request $request, $name)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Board')->findOneByName($name);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Board entity.');
        }

        $deleteForm = $this->createDeleteForm($name);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('board_edit', array('name' => $entity->getName())));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Board entity.
     *
     * @Route("/{name}", name="board_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $name)
    {
        $form = $this->createDeleteForm($name);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UebUebOfThingsBundle:Board')->findOneByName($name);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Board entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('board'));
    }

    /**
     * Creates a form to delete a Board entity by id.
     *
     * @param mixed $name The entity name
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($name)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('board_delete', array('name' => $name)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * Program Board
     * 
     * @Route("/{name}/sketch", name="board_program")
     * @Method("GET")
     * @Template()
     */
    public function programAction($name) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Board')->findOneByName($name);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Board entity.');
        }

        $form = $this->createForm(new \Ueb\UebOfThingsBundle\Form\SketchType(), $entity->getSketch());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->generateUrl('homepage'));
        $breadcrumbs->addItem("Boards", $this->generateUrl('board'));
        $breadcrumbs->addItem($entity->getName(), $this->generateUrl('board_show',array('name' => $entity->getName())));
        $breadcrumbs->addItem("Programar");
        
        return array(
            'board' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Upload  Program/Sketch
     * 
     * @Route("/{name}/sketch", name="board_upload")
     * @Method("POST")
     * 
     */
    public function uploadAction(Request $request, $name) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UebUebOfThingsBundle:Board')->findOneByName($name);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Board entity.');
        }

        $form = $this->createForm(new \Ueb\UebOfThingsBundle\Form\SketchType(), $entity->getSketch());
        $form->handleRequest($request);

        if ($form->get('cancel')->isClicked()) {

            return $this->redirect($this->generateUrl('board_show', array(
                                'name' => $entity->getName()
            )));
        }
        
        if ($form->get('upload')->isClicked()) {

            $this->upload($entity);
            return ;
        }

        if ($form->isValid()) {

            $arduinotools = $this->get('arduino_tools');
            $sketch = $form->getData();
            $buildoutput = $arduinotools->program($entity->getName(), $entity->getArduino()->getName(), $sketch->getCode());
            

            if ($buildoutput["STATE"] == "SUCCESS") {
                $entity->setSketch($sketch);
                $em->persist($entity);
                $em->flush();
                return new JsonResponse(array(
                    "success" => true,
                    "output" => $buildoutput
                ));
            }

            return new JsonResponse(array(
                "success" => false,
                "output" => $buildoutput
            ));
        }

        return $this->render('UebUebOfThingsBundle:Board:program.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }
    
    //Aux function
    protected function upload($board) {
        
        $arduinotools = $this->get('arduino_tools');
        $arduinotools->upload($board->getArduino());
        
        
    }
    
}
