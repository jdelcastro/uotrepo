<?php

namespace Ueb\UebOfThingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NavigationController extends Controller
{
    
    public function renderNavBarAction () {
        
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('UebUebOfThingsBundle:Board')->findAll();
        
        return $this->render(
                'UebUebOfThingsBundle:Default:nav.html.twig',
                array ('boards' => $entities)
        );
    }
    
    public function userMenuAction() {
        
//        $em = $this->getDoctrine()->getManager();
//        
//        $notifications = $em->getRepository('UebUebOfThingsBundle:Notification')->findUserNotificationsOrderedByTime($this->getUser()->getId());
        
        return $this->render(
                'UebUebOfThingsBundle:Default:userMenu.html.twig',
                array (
                    'user' => $this->getUser()
                )
        );
    }
    
}
