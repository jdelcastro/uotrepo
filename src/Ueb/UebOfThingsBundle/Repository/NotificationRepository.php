<?php

namespace Ueb\UebOfThingsBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of NotificationRepository
 *
 * @author joao
 */
class NotificationRepository extends EntityRepository {
    
    public function findUserNotificationsOrderedByTime($user_id) {
        
        $query = $this->getEntityManager()
                ->createQuery(
                        'SELECT n FROM UebUebOfThingsBundle:Notification n '
                        . 'JOIN n.user u '
                        . 'WHERE u.id = :uid  ORDER BY n.feeddata.time'
                )->setParameter('uid', $user_id);
        
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
}
