<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArduinoBuilder
 *
 * @author joao
 */


namespace Ueb\UebOfThingsBundle\ArduinoTools;

class ArduinoBuilder {
    
    private $makefile_values;
    private $current_dir;
    
    public function __construct() {
        $this->makefile_values = array(
            'ARDMK_DIR' => __DIR__ . '/arduino-tools/Arduino-Makefile',
            'ARDUINO_DIR' => __DIR__ . '/arduino-tools/arduino',
            'AVR_TOOLS_DIR' => __DIR__ . '/arduino-tools/arduino/hardware/tools/avr',
            'CURRENT_DIR' => '$(shell basename $(CURDIR))',
            'OBJDIR' => '$(CURRENT_DIR)/bin'
        );

        $this->current_dir = __DIR__ . '/sketches/';
    }

    public function build($name, $boardinfo, $sketch) {
        
        $dir = $this->current_dir . $name;
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777, true)) {
                die('Failed to create sketch folders...');
            }
        }
        
        if (!$this->createMakefile($boardinfo, $dir) === FALSE) {
            file_put_contents($dir.'/'.$name.'.ino', $sketch);
            return $this->makeBuild($dir);
        }
        
        return array("ERROR" => "INTERNAL");
    }

    private function createMakefile($boardinfo, $dir) {

        $contents = "";

        foreach ($this->makefile_values as $property => $value) {
            $contents .= $property . " = " . $value . "\n";
        }
        
        //add the board tag
        $contents .= "BOARD_TAG = ".$boardinfo."\n";

        //include $(ARDMK_DIR)/Arduino.mk --put this allways in the end of the child in the makefile
        if ($contents != "") {
            $contents .= "include $(ARDMK_DIR)/Arduino.mk";
            if (!file_put_contents($dir . '/Makefile', $contents) === FALSE) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * 
     * @param type $dir
     * @return type
     */
    private function makeBuild($dir) {

        $process = new \Symfony\Component\Process\Process("cd " . $dir . " && make");
        $process->run();


        if (!$process->isSuccessful()) {
            return array("STATE" => "ERROR", "OUTPUT" => $process->getErrorOutput());
        }

        return array("STATE" => "SUCCESS", "OUTPUT" => $process->getOutput());
    }
    
    public function getSource($board) {
        $filepath = $this->current_dir.$board."/".$board.".ino";
        $source = "";
        if (file_exists($filepath)) {
            $source = file_get_contents($filepath);
        }
        return $source;
    }
    
    public function cleanBuild($board) {
        
        $filepath = $this->current_dir.$board;
        $this->rrmdir($filepath);
    }
    
    private function rrmdir($dir) {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file)) {
                $this->rrmdir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dir);
    }

}
