<?php

namespace Ueb\UebOfThingsBundle\ArduinoTools;

class ArduinoTools {
    
    private $arduinobuilder;
    
    private $parameters;

    public function __construct() {
        $this->parameters = array(
            'delimiter' => "##############################################################",
            'path' => 'arduino-tools/arduino/hardware/arduino/boards.txt'
        );
        $this->arduinobuilder = new ArduinoBuilder();
    }

    public function getBoardInfo($name) {
        $lines = $this->parseTxt();

        $response = array();
        foreach ($lines as $line) {
            if (strpos($name, $line) !== false) {
                $property = str_replace($name . '.', "", $line);
                $value = explode($line, $line)[1];
                $response[$property] = $value;
            }
        }
        
        return $response;
    }
    
    public function getInfo($parameter = 'name')  {
        $lines = $this->parseTxt();
        
        $response = array();
        foreach ($lines as $line) {
            if (strpos($line, $parameter)!== false) {
                $name = explode('.', $line)[0];
                $description = explode('=', $line)[1];
                $response[$name] = $description;
            }
        }
        return $response;
    }
    
    protected function parseTxt() {
        
         $lines = file(__DIR__.'/'.$this->parameters['path']);
         
         $response = array();
         foreach($lines as $line) {
             if($line!=$this->parameters['delimiter']) {
                 $response []= trim(preg_replace('/\s+/', ' ', $line));
             }
         }
         
         return $response;
    }
    
    public function program($name, $board, $sketch) {

        return $this->arduinobuilder->build($name, $board, $sketch);
    }
            
    public function upload($board) {
        throw new Exception("Not Implemented Yet!!");
    }
    
    public function getSourceCode($board) {
        
        return $this->arduinobuilder->getSource($board);
    }
    
    public function clean($board) {
        $this->arduinobuilder->cleanBuild($board);
    }
          
}
