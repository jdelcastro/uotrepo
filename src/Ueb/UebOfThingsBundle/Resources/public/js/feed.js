function draw(_data) {
    
    var data = [];
    var i = 0;
    
    _data.data.forEach(function(item){
        data.push([parseDate(item.time), item.value]);
    });
    
    var options = {
            series: {
                lines: {show: true}
            },
            grid: {
                hoverable: true //IMPORTANT! this is needed for tooltip to work
            },
            
            yaxis: {
                tickFormatter: function(v, axis) {
                    return v;
                }
            },
            xaxis: {
                mode: 'time',
                timeformat: "%y/%m/%d %h:%m",
                tickFormatter: function(v, axis) {
                    return new Date(v);
                }
            },
            tooltip: true,
            tooltipOpts: {
                content: "%x - %y "+_data.measureunit,
                shifts: {
                       x: -60,
                    y: 25
                }
            },
            navigationControl: {
                homeRange: {xmin:-10,xmax:10,ymin:-10,ymax:10},
                panAmount: 100,
                zoomAmount: 1.5,
                position: {left: "20px", top: "20px"}
            }
        }
    
    $.plot($("#flot-chart-line"),[{data: data}],options);
    
}

function parseDate(date) {
    
    
    var arr = date.split(" ");
    var data = arr[0].split("-");
    var time = arr[1].split(":");
    
    //new DAte(year,month,days,hours,minutes,seconds)
    var finalDate = new Date(data[0], data[1]-1, data[2], time[0], time[1], time[2]);
    
    return finalDate;
    
}

function GetData() {
    //set no cache
    $.ajaxSetup({ cache: false });
    
    $('#loading').show();
 
    $.ajax({
        url: ajax_url,
        dataType: 'json',
        success: draw,  //if success, call draw()
        error: function () {
            //if fail
            alert("Error loading Graph!");
        },
        done: function() {
            alert("done");
          $('#loading').hide();
        }
    });
}
$(document).ready(function() {
    GetData();
});

//$(document).ready(function() {
//    //console.log("document ready");
//    var offset = 0;
//    var data = [];
//    plot();
//    function plot() {
//        for (var i = 0; i < 12; i += 0.2) {
//            data.push([i, Math.cos(i + offset)]);
//        }
//        var options = {
//            series: {
//                lines: {show: true},
//                points: {show: true}
//            },
//            grid: {
//                hoverable: true //IMPORTANT! this is needed for tooltip to work
//            },
//            yaxis: {min: -1.2, max: 1.2},
//            tooltip: true,
//            tooltipOpts: {
//                content: "'%s' of %x.1 is %y.4",
//                shifts: {
//                    x: -60,
//                    y: 25
//                }
//            }
//        };
//        var plotObj = $.plot($("#flot-chart-line"),
//                [    {data: data}],
//                options);
//    }
//});





