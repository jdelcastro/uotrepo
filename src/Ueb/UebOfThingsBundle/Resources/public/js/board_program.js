$(document).ready(function() {

    

    var forms = [
        '[ name="ueb_uebofthingsbundle_sketch"]'
    ];

    $(forms.join(',')).submit(function(e) {
        e.preventDefault();

        postForm($(this), function(response) {

            if (response.success !== false) {
                $("#output").attr("class", "alert alert-success");
                $("#ueb_uebofthingsbundle_sketch_upload").attr("disabled", false);

                //Verbose Output
                $("#output").text(response.output.OUTPUT);

                //Non-Verbose Output
                //$("#output").text("Compiled Successfully!!");
            } else {
                $("#output").attr("class", "alert alert-danger");
                $("#ueb_uebofthingsbundle_sketch_upload").attr("disabled", true);
                $("#output").text(response.output.OUTPUT);
            }
            ;


        });

        return false;
    });

    
    var codetextarea= document.getElementById("ueb_uebofthingsbundle_sketch_code")
    var myCodeMirror = CodeMirror.fromTextArea(codetextarea , {
        lineNumbers: true,
        matchBrackets: true,
        mode: 'text/x-csrc'
    });
    
    myCodeMirror.on('change', function(cmirror) {
        codetextarea.value = cmirror.getValue();
    });

});

