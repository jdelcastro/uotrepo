
// When sport gets selected ...
$(document.body).on('change','#ueb_uebofthingsbundle_feed_thing', function() {
  // ... retrieve the corresponding form.
  var $form = $(this).closest('form');
  // Simulate form data, but only include the selected thing value.
  var data = {};
  data[$(this).attr('name')] = $(this).val();
  // Submit data via AJAX to the form's action path.
  $.ajax({
    url : $form.attr('action'),
    type: $form.attr('method'),
    data : data,
    success: function(html) {
      // Replace current field ...
      $form.replaceWith(
    // ... with the returned one from the AJAX response.
        $(html).find('#ueb_uebofthingsbundle_feed_thing').closest('form')
      );
      // Position field now displays the appropriate positions.
    }
  });
});