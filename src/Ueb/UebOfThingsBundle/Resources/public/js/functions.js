
function breadCrumbsNotification(notification) {
    var obj = jQuery.parseJSON(notification);

    var alert_div = null;
    var html = null;
    
    var name = '<a href="' + obj.feed_url +'"> ' + obj.feed_name + "</a>";
    

    if (obj.notification_type == "critical") {

        html = "<h4>Crítico! </h4>" + name + " com valor a " + obj.value + " " + " " + obj.measure;

        alert_div = $("<div/>", {
            "class": "alert alert-dismissable alert-danger"
        });
    } else if (obj.notification_type == "warning") {

        html = "<h4>Alerta! </h4>" + name + " com valor a " + obj.value + " " + " " + obj.measure;

        alert_div = $("<div/>", {
            "class": "alert alert-dismissable alert-warning"
        });
    }
    ;

    if (alert_div != null) {
        var close_button = $("<button/>", {
            html: "<small>x</small>",
            "class": "close",
            "data-dismiss": "alert",
            "type": "button"
        }).appendTo(alert_div);


        alert_div.append(html);

        alert_div.appendTo("#notification");
    }
    ;


}

function alerts(notification) {

    var obj = jQuery.parseJSON(notification);

    var alert_li = $("<li/>");


    var alert_a = $("<a/>", {
        html: obj.feed_name,
        "href": obj.feed_url
    }).appendTo(alert_li);

    var alert_span = null;

    if (obj.notification_type == "critical") {
        alert_span = $("<span/>", {
            html: obj.value + " " + obj.measure,
            "class": "label label-danger"
        });
    } else if (obj.notification_type == "warning") {
        alert_span = $("<span/>", {
            html: obj.value + " " + obj.measure,
            "class": "label label-warning"
        });
    }

    if (alert_span != null) {
        alert_span.appendTo(alert_a);
        alert_a.appendTo(alert_li);
        alert_li.insertBefore("#divider_alerts");
        var badge_value = parseInt($(".dropdown-toggle .badge").text())+1;
        $(".dropdown-toggle .badge").text(badge_value);
    }
    
    
    
}

function cleanAlerts(ajax_url) {
    
    $.ajax({
        url: ajax_url,
        type: "POST",
        success: clean,  //if success, call draw()
        error: function () {
            //if fail
            alert("Erro");
        }
    });
    
}

function clean() {
    
    $("#user-notifications-dropdown li").each(function(_, node) {
        if($(node).attr("class")==="divider") {
            return false;
        }
        $(node).remove();
    });
    
    $(".dropdown-toggle .badge").text(0);
    
}

