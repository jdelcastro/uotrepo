$(document).ready(function() {
    $('#datatable1').dataTable({
         "language": {
            "lengthMenu": "_MENU_ items por página",
            "search": "Procurar: ",
            "zeroRecords": "Não existens items para mostrar",
            "info": "Mostrando _PAGE_ of _PAGES_",
            "infoEmpty": "Não encontrado",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "next": "Seguinte",
                "previous": "Anterior"
            }
        }
    });
    
    $('#datatable2').dataTable({
         "language": {
            "lengthMenu": "_MENU_ items por página",
            "search": "Procurar: ",
            "zeroRecords": "Não existens items para mostrar",
            "info": "Mostrando _PAGE_ of _PAGES_",
            "infoEmpty": "Não encontrado",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "next": "Seguinte",
                "previous": "Anterior"
            }
        }
    });
} );