<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoadFeedFixtures
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\DataFixtures\ORM;

use Ueb\UebOfThingsBundle\Entity\Feed;
use Ueb\UebOfThingsBundle\Entity\FeedData;
use Ueb\UebOfThingsBundle\Entity\Thing;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class loadFeedData implements FixtureInterface {
    
    
    public function load(ObjectManager $manager) {
        
        
        $sensor = new Thing();
        $sensor->setName("Sensor");
        $sensor->setType(Thing::SENSOR);
        
        $actuator = new Thing();
        $actuator->setName("Atuador");
        $actuator->setType(Thing::ACTUATOR);
        
        $feed1 = new Feed();
        $feed1->setName("TemperaturaFeed1");
        $feed1->setMac("000000001");
        $feed1->setCriticalVaue(20);
        $feed1->setWarningValue(10);
        $feed1->setMeasureunit("Cº");
        $feed1->setThing($sensor);
        
        $feed2 = new Feed();
        $feed2->setName("Rele");
        $feed2->setMac("123456789");
        $feed2->setThing($actuator);
        
//        $data= new \DateTime();
//        $interval = new \DateInterval('P10D');
//        $data->sub($interval);
//        for ($i = 0; $i<1764*5; $i++) {
//            $new_data = clone $data ;
//            $feed_data = new FeedData();
//            $feed_data->setValue(rand(1, 21));
//            $feed_data->setTime($new_data);
//            $feed1->addDatum($feed_data);
//            $minutes_to_add = 5;
//            $data->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
//            
//        }
        
        $manager->persist($sensor);
        $manager->persist($actuator);
        $manager->persist($feed1);
        $manager->persist($feed2);
        $manager->flush();
        
    }

 

}
