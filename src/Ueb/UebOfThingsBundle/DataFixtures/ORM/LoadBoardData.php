<?php

namespace Ueb\UebOfThingsBundle\DataFixtures\ORM;

use Ueb\UebOfThingsBundle\Entity\Board;
use Ueb\UebOfThingsBundle\Entity\Arduino;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class loadBoardData implements FixtureInterface, ContainerAwareInterface  {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        
//        $em = $this->container->get('doctrine')->getEntityManager();
        
        $atservice = $this->container->get('arduino_tools');
        $boards = $atservice->getInfo() ;
        
        foreach ($boards as $key => $value) {
            $b = new Arduino();
            $b->setName($key);
            $b->setDescription($value);
            
            $manager->persist($b);
        }
        
        $manager->flush();
        
        $board1 = new Board();
        $board1->setName("Eth");
        $board1->setDescription("Arduino Board com Ethernet");  
        $a1 = $manager->getRepository('UebUebOfThingsBundle:Arduino')->findOneByName("uno");
        $board1->setArduino($a1);
        
        
        $board2 = new Board();
        $board2->setName("Temperaturas");
        $board2->setDescription("Arduino Board responsavel pelas temperaturas");  
        $a2 = $manager->getRepository('UebUebOfThingsBundle:Arduino')->findOneByName("mega");
        $board2->setArduino($a2);
        
        $board3 = new Board();
        $board3->setName("Humidade");
        $board3->setDescription("Arduino Board responsavel pela humidade");  
        $a3 = $manager->getRepository('UebUebOfThingsBundle:Arduino')->findOneByName("mini");
        $board3->setArduino($a3);
        
        
        $defaulttype = new \Ueb\UebOfThingsBundle\Entity\NodeType();
        $defaulttype->setName("Default");
        
        $isptype = new \Ueb\UebOfThingsBundle\Entity\NodeType();
        $isptype->setName("ISP");
        
        $board1->setType($defaulttype);
        $board2->setType($defaulttype);
        $board3->setType($defaulttype);
        $manager->persist($board1);
        $manager->persist($board2);
        $manager->persist($board3);
        
        $manager->persist($defaulttype);
        $manager->persist($isptype);
        
        
        $manager->flush();
        
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null) {
        $this->container = $container;
    }

}