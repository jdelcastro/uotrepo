<?php

/**
 * Description of User
 *
 * @author joao
 */
namespace Ueb\UebOfThingsBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User implements UserInterface {
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     * 
     */
    private $username;
    
    
    /**
     *
     * @var string 
     * 
        * @ORM\Column(name="password", type="string", length=255)
     * 
     */
    private $password;
    
    /**
     *
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Notification")
     * @ORM\JoinTable(name="users_notifications",
     *          joinColumns={@ORM\JoinColumn("user_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="notification_id", referencedColumnName="id")})
     */
    private $notifications;
    
    /**
     *
     * @ORM\Column(name="celnumber", type="string", length=9, nullable=true)
     */
    private $celNumber;
    
    /**
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true) 
     */
    private $email;
    
    public function __construct() {
        $this->notifications = new ArrayCollection();
    }
    
    public function eraseCredentials() {
        
    }

    public function getPassword() {
        return $this->password;
    }

    public function getRoles() {
        return array('ROLE_ADMIN');
    }

    public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->username;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Add notifications
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Notification $notifications
     * @return User
     */
    public function addNotification(\Ueb\UebOfThingsBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Notification $notifications
     */
    public function removeNotification(\Ueb\UebOfThingsBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
    
    /**
     * Remove all ntoification
     *
     * 
     */
    public function removeNotifications()
    {
        $this->notifications->clear();
    }

    /**
     * Set celNumber
     *
     * @param string $celNumber
     * @return User
     */
    public function setCelNumber($celNumber)
    {
        $this->celNumber = $celNumber;

        return $this;
    }

    /**
     * Get celNumber
     *
     * @return string 
     */
    public function getCelNumber()
    {
        return $this->celNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
}
