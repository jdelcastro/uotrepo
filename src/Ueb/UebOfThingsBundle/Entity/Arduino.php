<?php

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Arduino
 *
 * @ORM\Table(name="arduino")
 * @ORM\Entity
 */
class Arduino
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Get names
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Get desctription
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }
    
    



    /**
     * Set name
     *
     * @param string $name
     * @return Arduino
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Arduino
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

   
    public function __toString() {
        return $this->description;
    }
    
    

}
