<?php

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * FeedGroup
 *
 * @ORM\Table(name="feed_group")
 * @ORM\Entity
 */
class FeedGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     *
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Feed", mappedBy="group")
     */
    private $feeds;
    
    public function __construct() {
        $this->feeds = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FeedGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add feeds
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Feed $feeds
     * @return FeedGroup
     */
    public function addFeed(\Ueb\UebOfThingsBundle\Entity\Feed $feeds)
    {
        $this->feeds[] = $feeds;

        return $this;
    }

    /**
     * Remove feeds
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Feed $feeds
     */
    public function removeFeed(\Ueb\UebOfThingsBundle\Entity\Feed $feeds)
    {
        $this->feeds->removeElement($feeds);
    }

    /**
     * Get feeds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeeds()
    {
        return $this->feeds;
    }
    
    public function __toString() {
        return $this->name;
    }
    
    
}
