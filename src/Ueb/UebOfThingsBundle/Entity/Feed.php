<?php

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Feed
 *
 * @ORM\Table(name="feed")
 * @ORM\Entity(repositoryClass="Ueb\UebOfThingsBundle\Repository\FeedRepository")
 */
class Feed {

    
    private static $s = 60;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false, unique=true)
     */
    private $name;
    

    /**
     * @var string
     *
     * @ORM\Column(name="mac", type="string", length=100, nullable=false, unique=true)
     */
    private $mac;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Board", inversedBy="feeds")
     * @ORM\JoinColumn(name="board_id", referencedColumnName="id")
     */
    private $board;

    /**
     * @ORM\OneToMany(targetEntity="FeedData", mappedBy="feed", cascade={"persist", "remove"}))
     **/
    private $data;

    /**
     *
     * @var Thing 
     * 
     * @ORM\ManyToOne(targetEntity="Thing")
     * @ORM\JoinColumn(name="thing_id", referencedColumnName="id")
     * 
     */
    private $thing;

    /**
     * 
     * @var float
     * 
     * @ORM\Column(name="warning_value", nullable=true, type="float")
     * 
     */
    private $warningValue;

    /**
     * 
     * @var float
     * 
     * @ORM\Column(name="critical_value", type="float",nullable=true)
     * 
     */
    private $criticalVaue;

    /**
     * 
     * @var string
     * 
     * @ORM\Column(name="measureunit", type="string", nullable=true, length=25)
     * 
     */
    private $measureunit;
    
    /**
     * 
     * @ORM\ManyToOne(targetEntity="FeedGroup", inversedBy="feeds")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;


    public function __construct() {
        $this->data = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Feed
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set mac
     *
     * @param string $mac
     * @return Feed
     */
    public function setMac($mac) {
        $this->mac = $mac;

        return $this;
    }

    /**
     * Get mac
     *
     * @return string 
     */
    public function getMac() {
        return $this->mac;
    }

    /**
     * Set board
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Board $board
     * @return Feed
     */
    public function setBoard(\Ueb\UebOfThingsBundle\Entity\Board $board = null) {
        $this->board = $board;

        return $this;
    }

    /**
     * Get board
     *
     * @return \Ueb\UebOfThingsBundle\Entity\Board 
     */
    public function getBoard() {
        return $this->board;
    }

    /**
     * Add data
     *
     * @param \Ueb\UebOfThingsBundle\Entity\FeedData $data
     * @return Feed
     */
    public function addDatum(\Ueb\UebOfThingsBundle\Entity\FeedData $data) {
        
        $data->setFeed($this);
        $this->data[] = $data;
        
        
        return $this;
    }

    /**
     * Remove data
     *
     * @param \Ueb\UebOfThingsBundle\Entity\FeedData $data
     */
    public function removeDatum(\Ueb\UebOfThingsBundle\Entity\FeedData $data) {
        
        $data->setFeed();
        $this->data->removeElement($data);
    }

    /**
     * Get data
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getData($period=null) {
        if(!$period) {
            return $this->data;
        }
        
        switch ($period) {
            case 'day':
  
                break;
            case 'week':

                break;
            case 'year':

                break;
            default:
                break;
        }
        
        return null;
    }

    /**
     * Set thing
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Thing $thing
     * @return Feed
     */
    public function setThing(\Ueb\UebOfThingsBundle\Entity\Thing $thing = null) {
        $this->thing = $thing;

        return $this;
    }

    /**
     * Get thing
     *
     * @return \Ueb\UebOfThingsBundle\Entity\Thing 
     */
    public function getThing() {
        return $this->thing;
    }

    /**
     * Set warningValue
     *
     * @param float $warningValue
     * @return Feed
     */
    public function setWarningValue($warningValue) {
        $this->warningValue = $warningValue;

        return $this;
    }

    /**
     * Get warningValue
     *
     * @return float 
     */
    public function getWarningValue() {
        return $this->warningValue;
    }

    /**
     * Set criticalVaue
     *
     * @param float $criticalVaue
     * @return Feed
     */
    public function setCriticalVaue($criticalVaue) {
        $this->criticalVaue = $criticalVaue;

        return $this;
    }

    /**
     * Get criticalVaue
     *
     * @return float 
     */
    public function getCriticalVaue() {
        return $this->criticalVaue;
    }

    /**
     * Set measureunit
     *
     * @param string $measureunit
     * @return Feed
     */
    public function setMeasureunit($measureunit) {
        $this->measureunit = $measureunit;

        return $this;
    }

    /**
     * Get measureunit
     *
     * @return string 
     */
    public function getMeasureunit() {
        return $this->measureunit;
    }
    
    public function getTypeOfThing() {
        return $this->thing->getType();
    }
    
    /**
     * Set group
     *
     * @param \Ueb\UebOfThingsBundle\Entity\FeedGroup $group
     * @return Feed
     */
    public function setGroup(\Ueb\UebOfThingsBundle\Entity\FeedGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Ueb\UebOfThingsBundle\Entity\FeedGroup 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Get last FeedData
     * 
     * @return FeedData
     */
    public function getCurrentData() {
        
        $last_value = $this->data->last();
        
        
        if($last_value) {
            
            $now = new \DateTime();
            
            $seconds = abs($now->getTimeStamp() - $last_value->getTime()->getTimeStamp());
            
            if($seconds > self::$s) {
                return null;
            }
            
            return $last_value;
        }
        return null;
    }    
    
    public function getCountNormalValues() {
        
        $count = 0 ;
        
        if (!$this->data->isEmpty()) {
            foreach ($this->data->toArray() as $data) {
                if($data->getValue() < $this->warningValue) {
                    $count++;
                }
            }
        }
        
        return $count;
        
    }
    
    public function getCountWarningValues() {
        
        $count = 0 ;
        
        if (!$this->data->isEmpty()) {
            foreach ($this->data->toArray() as $data) {
                if($data->getValue() >= $this->warningValue && $data->getValue()< $this->criticalVaue) {
                    $count++;
                }
            }
        }
        
        return $count;
        
    }
    
    public function getCountCriticalValues() {
        
        $count = 0 ;
        
        if (!$this->data->isEmpty()) {
            foreach ($this->data->toArray() as $data) {
                if($data->getValue() >= $this->criticalVaue) {
                    $count++;
                }
            }
        }
        
        return $count;
        
    }
    
    
    /**
     * 
     * @Assert\Callback
     */
    public function isBoardAbleToHaveThisFeed(ExecutionContextInterface $context) {

        if ($this->board) {
            if (!$this->board->canHaveFeeds()) {
                $context->addViolationAt("board", "Feed cannot be associated here!");
            }
        }
    }
    
    public function __toString() {
        return $this->name;
    }

    
}
