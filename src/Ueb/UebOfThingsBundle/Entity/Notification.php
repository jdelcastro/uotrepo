<?php



/**
 * Description of Notification
 *
 * @author joao
 */

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeedData
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="Ueb\UebOfThingsBundle\Repository\NotificationRepository")
 */
class Notification {
    
    const WARNING = 0;
    const CRITICAL = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var type
     * 
     * @ORM\OneToOne(targetEntity="FeedData") 
     * @ORM\JoinColumn(name="feeddata_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $feeddata;
    
    
    /**
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set feeddata
     *
     * @param \Ueb\UebOfThingsBundle\Entity\FeedData $feeddata
     * @return Notification
     */
    public function setFeeddata(\Ueb\UebOfThingsBundle\Entity\FeedData $feeddata = null)
    {
        $this->feeddata = $feeddata;

        return $this;
    }

    /**
     * Get feeddata
     *
     * @return \Ueb\UebOfThingsBundle\Entity\FeedData 
     */
    public function getFeeddata()
    {
        return $this->feeddata;
    }

    
    

    /**
     * Set type
     *
     * @param integer $type
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
    
    public function getTypeToString() {
        switch ($this->type) {
            case self::WARNING:
                    return "warning";
            case self::CRITICAL:
                    return "critical";
            default: 
                return null;
        }
    }
}
