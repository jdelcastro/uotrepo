<?php

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sketch
 *
 * @ORM\Table(name="sketch")
 * @ORM\Entity
 */
class Sketch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false) 
     */
    private $name;
    
    private $code;

    /**
     *
     * @ORM\OneToOne(targetEntity="Board", mappedBy="sketch")
     * 
     */
    private $board;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get board
     *
     * @return \Ueb\UebOfThingsBundle\Entity\Board 
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Sketch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Return code located in $path
     */
    public function getCode() {
        
        return $this->code;
    }
    
    /**
     * 
     */
    public function setCode($code) {
        $this->code = $code;
    }
    
    
    
    

    /**
     * Set node
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Board $board
     * @return Sketch
     */
    public function setBoard(\Ueb\UebOfThingsBundle\Entity\Board $board = null)
    {
        $this->board = $board;

        return $this;
    }
    
    /**
     * To String
     * 
     */
    public function __toString() {
        return "";
    }
}
