<?php

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Thing
 *
 * @ORM\Table(name="thing")
 * @ORM\Entity
 */
class Thing
{
    
    const SENSOR = 0;
    const ACTUATOR = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="name", type="string", length=100)
     * 
     */
    private $name;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="type", type="integer")
     * 
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Thing
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param int $type
     * @return Thing
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int 
     */
    public function getType() {
        return $this->type;
    }
    
    public function __toString() {
        switch ($this->type) {
            case self::ACTUATOR:
                return "Atuador ".$this->name;
            case self::SENSOR:
                return "Sensor ".$this->name;
            
            default:
                return "";
        };
    }

}
