<?php

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Board
 *
 * @ORM\Table(name="board")
 * @ORM\Entity(repositoryClass="Ueb\UebOfThingsBundle\Repository\BoardRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Board
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string
     * 
     * @ORM\Column(name="name", type="string", length=100, unique=true) 
     */
    private $name;
    
    /**
     * 
     * @return integer
     * 
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="Sketch", inversedBy="board", cascade={"persist", "remove"})
     * 
     */
    private $sketch;
    
    
    /**
     *
     * @var type 
     * 
     * @ORM\ManyToOne(targetEntity="Arduino")
     * @ORM\JoinColumn(name="arduino_id", referencedColumnName="id", nullable=false)
     */
    private $arduino;
    
    /**
     * @var NodeType
     * 
     * @ORM\ManyToOne(targetEntity="NodeType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private $type;
    
    /**
     *
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Feed", mappedBy="board") 
     */
    private $feeds;
    
    /**
     *
     * @var Board
     * 
     * @ORM\OneToOne(targetEntity="Board")
     * @ORM\JoinColumn(name="isp_programmer", referencedColumnName="id")
     * 
     */
    private $programmer;
    
    public function __construct() {
        $this->feeds = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
 
    /**
     * Set name
     *
     * @param string $name
     * @return Board
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Board
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sketch
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Sketch $sketch
     * @return Board
     */
    public function setSketch(\Ueb\UebOfThingsBundle\Entity\Sketch $sketch = null)
    {
        $this->sketch = $sketch;

        return $this;
    }

    /**
     * Get sketch
     *
     * @return \Ueb\UebOfThingsBundle\Entity\Sketch 
     */
    public function getSketch()
    {
        return $this->sketch;
    }

    /**
     * Set arduino
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Arduino $arduino
     * @return Board
     */
    public function setArduino(\Ueb\UebOfThingsBundle\Entity\Arduino $arduino)
    {
        $this->arduino = $arduino;

        return $this;
    }

    /**
     * Get arduino
     *
     * @return \Ueb\UebOfThingsBundle\Entity\Arduino 
     */
    public function getArduino()
    {
        return $this->arduino;
    }

    /**
     * Add feeds
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Feed $feeds
     * @return Board
     */
    public function addFeed(\Ueb\UebOfThingsBundle\Entity\Feed $feeds)
    {
        $this->feeds[] = $feeds;

        return $this;
    }

    /**
     * Remove feeds
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Feed $feeds
     */
    public function removeFeed(\Ueb\UebOfThingsBundle\Entity\Feed $feeds)
    {
        $this->feeds->removeElement($feeds);
    }

    /**
     * Get feeds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeeds()
    {
        return $this->feeds;
    }
    
    public function __toString() {
        return $this->name." - ".$this->arduino->getDescription();  
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Board
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set programmer
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Board $programmer
     * @return Board
     */
    public function setProgrammer(\Ueb\UebOfThingsBundle\Entity\Board $programmer = null)
    {
        $this->programmer = $programmer;

        return $this;
    }

    /**
     * Get programmer
     *
     * @return \Ueb\UebOfThingsBundle\Entity\Board 
     */
    public function getProgrammer()
    {
        return $this->programmer;
    }
    
    
    public function canHaveFeeds(){
        if($this->type == "Default") {
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @Assert\Callback
     */
    public function isThisBoardAbleToHaveFeeds(ExecutionContextInterface $context) {
        
        if(!$this->feeds->isEmpty() && $this->type == "ISP") {
            $context->addViolationAt("type", "ISP Board cannot have associated Feeds!");
        }
    }
    
    /**
     * 
     * @ORM\PreRemove
     */
    public function removeFeedsAssociation() {
        $feeds = $this->feeds->toArray();
        foreach ($feeds as $feed) {
            $this->removeFeed($feed);
            $feed->setBoard();
        }
    }
}
