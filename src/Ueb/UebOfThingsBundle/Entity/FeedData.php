<?php

namespace Ueb\UebOfThingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeedData
 *
 * @ORM\Table(name="feeddata")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FeedData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var DateTime 
     * 
     * @ORM\Column(name="time", type="datetime")
     * 
     */
    private $time;
    
            
            
    /**
     * 
     * @var float
     * 
     * @ORM\Column(name="value", type="float")
     * 
     */
    private $value;
    
    /**
     * @var feed
     * 
     * @ORM\ManyToOne(targetEntity="Feed", inversedBy="data")
     * @ORM\JoinColumn(name="feed_id", referencedColumnName="id")
     */
    private $feed;
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return FeedData
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime()
    {
        return $this->time;
    }


    /**
     * Set value
     *
     * @param float $value
     * @return FeedData
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * Set feed
     *
     * @param \Ueb\UebOfThingsBundle\Entity\Feed $feed
     * @return FeedData
     */
    public function setFeed(\Ueb\UebOfThingsBundle\Entity\Feed $feed = null)
    {
        $this->feed = $feed;

        return $this;
    }

    /**
     * Get feed
     *
     * @return \Ueb\UebOfThingsBundle\Entity\Feed 
     */
    public function getFeed()
    {
        return $this->feed;
    }
    
    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        
        if(!$this->time){
            $this->time = new \DateTime();
        }
            
    }
}
