<?php

namespace Ueb\UebOfThingsBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FeedRepositoryFunctionalTest extends WebTestCase {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setUp() {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
                ->get('doctrine')
                ->getManager()
        ;
    }

    public function testFindFeedJoinedByFeedData() {
        $feed = $this->em
                ->getRepository('UebUebOfThingsBundle:Feed')
                ->findFeedJoinedByFeedData(5, 'week')
        ;

        var_dump($feed);
            
        $this->assertNotEquals(null, $feed);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown() {
        parent::tearDown();
        $this->em->close();
    }

}
