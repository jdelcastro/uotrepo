/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function postForm($form, callback) {

    /*
     * Get all form values
     */
    var values = {};
    $.each($form.serializeArray(), function(i, field) {
        values[field.name] = field.value;
    });

    /*
     * Throw the form values to the server!
     */
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: values,
        success: function(data) {
            callback(data);
        }
    });

}
